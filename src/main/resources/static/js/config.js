'use strict'

angular.module('testApp' , ["ngRoute"])
       .config(function($routeProvider){

            $routeProvider
            .when('/' , {
                templateUrl : "views/main.html",
                controller : 'MainController',
                controllerAs : 'main'
            })
            .when('/viewOne' , {
                templateUrl : "views/viewOne.html",
                controller : 'viewOneController',
                controllerAs : 'viewOne'
            })
            .when('/viewTwo' , {
                 templateUrl : "views/viewTwo.html",
                 controller : 'viewTwoController',
                 controllerAs : 'viewTwo'
            })
            .otherwise({
                    redirectTo: '/'
            });
       });