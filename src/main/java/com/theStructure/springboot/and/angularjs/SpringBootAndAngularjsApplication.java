package com.theStructure.springboot.and.angularjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAndAngularjsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAndAngularjsApplication.class, args);
	}
}
